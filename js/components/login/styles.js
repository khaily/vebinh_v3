
const React = require('react-native');

const { StyleSheet, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;

module.exports = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  input: {
    marginBottom: 20,
  },
  view:{
    marginTop: deviceHeight / 13,
  },
  bg: {
    flex: 1,
    marginTop: deviceHeight / 9,
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 30,
    bottom: 0,
  },
  viewinput:{
    flex:1
  },
  text:{
    fontSize :28,
    //color:'#fffff0',
    alignSelf: 'center'
  },
  buttonview:{
    flexDirection:'row',
    flex:1,
    alignItems: 'center',
    justifyContent:'center'
  }
});
