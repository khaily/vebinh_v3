
import React, { Component } from 'react';
import { Image,  TextInput,TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, InputGroup, Input, Button, Icon, View,Text,Thumbnail } from 'native-base';

import { replaceRoute } from '../../actions/route';
import { setUser } from '../../actions/user';
import styles from './styles';

import feathers from 'feathers/client'
import hooks from 'feathers-hooks';
import socketio from 'feathers-socketio/client'
import authentication from 'feathers-authentication/client';

if (window.navigator && Object.keys(window.navigator).length == 0) {
  window = Object.assign(window, {navigator: {userAgent: 'ReactNative'}});
}
var io = require('socket.io-client/socket.io');
const thumbnail = require('../../../images/1.png');
class Login extends Component {
  static propTypes = {
    setUser: React.PropTypes.func,
    replaceRoute: React.PropTypes.func,
  }
  _passPropSignup(){
    this.props.navigator.push({
      id:'form',
      passProps:{
          app:this.app,
      }
    });
  }
  _passPropHome(){
    this.setUser(this.state.email)
    this.props.replaceRoute('post');
  }
  setUser(name) {
   this.props.setUser(name);
  }
  _login() {
    this.app.authenticate({
      type: 'local',
      'email': this.state.email,
      'password': this.state.password
    }).then(this._passPropHome())
      .catch(function(error)
        {
              console.error('Error authenticating!', error);
        }
      );
  }
  constructor(props) {
    super(props);

    const options = {transports: ['websocket'], forceNew: true};
    const socket = io('http://192.168.23.1:3030', options);
    this.app = feathers()
      .configure(socketio(socket))
      .configure(hooks())
      .configure(authentication({
        storage: window.localStorage
      }));
    this.state = {
        email:'',
        password:'',
      };
  }
  render() {
    return (
      <Container>
        <View style={styles.container}>
          <Content>
              <View style={styles.view}>
                <View>
                  <Text style={styles.text}>Welcome</Text>
                </View>
                <View>
                  <Thumbnail size={100} source={thumbnail} style={{ alignSelf: 'center'}} />
                </View>
              </View>
              <View style={styles.bg}>
                  <InputGroup style={styles.input}>
                    <Icon name="ios-person" />
                    <Input
                      placeholder="Email"
                      onChangeText={email => this.setState({ email })}
                      value={this.state.email}
                    />
                  </InputGroup>
                  <InputGroup style={styles.input}>
                    <Icon name="ios-unlock-outline" />
                    <Input
                      placeholder="Password"
                      secureTextEntry
                      onChangeText={(password)=>this.setState({password})}
                      value={this.state.password}
                    />
                  </InputGroup>
                  <View>
                    <TouchableOpacity style={{alignSelf:'center'}} >
                      <Text>
                        Forgot Password?
                      </Text>
                    </TouchableOpacity>
                  </View>
                <View>
                  <Button  block onPress={() => this._login()}>
                    Sign In
                  </Button>
                  <View style={{flexDirection:'row',alignSelf:'center',marginTop:20}}>
                    <Text>
                      Dont have an account yet?
                    </Text>
                    <TouchableOpacity onPress={()=>this._passPropSignup()}>
                      <Text>
                        <Text> </Text>Sign Up
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

          </Content>
        </View>
      </Container>
    );
  }
}

function bindActions(dispatch) {
  return {
    setUser: name => dispatch(setUser(name)),
    replaceRoute: route => dispatch(replaceRoute(route)),
  };
}

export default connect(null, bindActions)(Login);
