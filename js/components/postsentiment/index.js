
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity,ListView,View } from 'react-native';
import { Container, Header, Title, Content, Text, Button, Icon } from 'native-base';
import { openDrawer } from '../../actions/drawer';
import { popRoute } from '../../actions/route';
import styles from './styles';

var api={
  getPostsentiments(){
    return fetch('http://192.168.23.1:3030/postsentiments',{method:"GET"})
        .then((response) => response.json())
         .then((responseJson) => {
            return responseJson.data;
         })
         .catch((error) => {
         console.error(error);
        });
  }
}
var ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!=r2});
class PostSentiment extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    name: React.PropTypes.string,
    openDrawer: React.PropTypes.func,
  }
  constructor(props){
    super(props);
    this.state={
      postsentiment:[],
    }
  }
  componentWillMount(){
    api.getPostsentiments().then((response)=>{
      console.log('ObjectId => ',response);
      this.setState({
        postsentiment:response,
      })
    });
  }
  popRoute() {
    this.props.popRoute();
  }
  renderRow(rowData,selectionId,rowID){
    return(
        <View>
          <View style={styles.Itemlistview}>
            <View style={styles.item} >
             <Text>{rowData.postid}</Text>
            </View>
            <View style={styles.item} >
             <Text>{rowData.author}</Text>
            </View>
          </View>
          <View style={styles.Itemlistview}>
              <View style={styles.item} >
               <Text>{rowData.sentiment}</Text>
              </View>
              <View style={styles.item}>
               <Text>{rowData.newsentiment}</Text>
              </View>
              <View style={styles.item}>
               <Text>{rowData.keyword}</Text>
              </View>
              <View style={styles.item}>
               <Text>{rowData.message}</Text>
              </View>
          </View>
        </View>
    )
  }

  render() {
    const { props:  name  } = this;
    console.log("postsentiment =>",this.state.postsentiment)
    return (
      <Container style={styles.container}>
        <Header>
          <Title>Post Sentiment</Title>

          <Button transparent onPress={this.props.openDrawer}>
            <Icon name="ios-menu" />
          </Button>
        </Header>

        <Content>
          <ListView
            dataSource={ds.cloneWithRows(this.state.postsentiment)}
            renderRow={this.renderRow.bind(this)}
          />
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    popRoute: () => dispatch(popRoute()),
    openDrawer: () => dispatch(openDrawer()),
  };
}

function mapStateToProps(state) {
  return {
    name: state.user.name,
  };
}

export default connect(mapStateToProps, bindAction)(PostSentiment);
