
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity,ListView } from 'react-native';
import { Container, Header, Title, Content, Button, Icon, List, ListItem, InputGroup, Input, Picker, Text, View, Thumbnail } from 'native-base';

import { openDrawer } from '../../actions/drawer';

import { replaceRoute,popRoute, } from '../../actions/route';

import styles from './styles';

var api={
  getPosts(){
    return fetch('http://192.168.23.1:3030/posts',{method:"GET"})
        .then((response) => response.json())
         .then((responseJson) => {
            return responseJson.data;
         })
         .catch((error) => {
         console.error(error);
        });
  }
}
var ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!=r2});
class Post extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
    name: React.PropTypes.string,

  }
  constructor(props){
    super(props);
    this.state={
      posts:[],
    }
  }
  componentWillMount(){
    api.getPosts().then((response)=>{
      console.log('ObjectId => ',response);
      this.setState({
        posts:response,
      })
    });
  }
  popRoute() {
    this.props.popRoute();
  }
  renderRow(rowData,selectionId,rowID){
    return(
        <TouchableOpacity onPress={()=> this.rowPressed('editpost',rowData)}>
          <View style={styles.Itemlistview}>
              <View style={styles.item} >
               <Text>{rowData.sentiment}</Text>
              </View>
              <View style={styles.item}>
               <Text>{rowData.keyword}</Text>
              </View>
              <View style={styles.item}>
               <Text>{rowData.message}</Text>
              </View>
          </View>
        </TouchableOpacity>
    )
  }
  rowPressed(route,rowData) {
    this.props.navigator.push({
      id:route,
      passProps:{
        data:rowData
      }
    })
  }
  render() {
    const { props: name } = this;
    return (
      <Container style={styles.container}>
        <Header>
          <Title>{(name) ? this.props.name : 'Blank Page'}</Title>
          <Button transparent onPress={this.props.openDrawer}>
            <Icon name="ios-menu" />
          </Button>
        </Header>

        <Content>
          <ListView
            dataSource={ds.cloneWithRows(this.state.posts)}
            renderRow={this.renderRow.bind(this)}
          />
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    popRoute: () => dispatch(popRoute()),
  };
}
function mapStateToProps(state) {
  return {
    name: state.user.name,
  };
}

export default connect(mapStateToProps, bindAction)(Post);
