
const React = require('react-native');

const { StyleSheet } = React;

module.exports = StyleSheet.create({
  container: {
    backgroundColor: '#FBFAFA',
  },
  Itemlistview:{
    flexDirection:'row',
    flex:1,
    padding :10,
    backgroundColor: '#DEE',
  },
  item:{
    paddingRight:10
  },
});
