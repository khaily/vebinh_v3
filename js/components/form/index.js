
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native';
import { Container, Header, Title, Content, Button, Icon, List, ListItem, InputGroup, Input, Picker, Text, View, Thumbnail } from 'native-base';

import { openDrawer } from '../../actions/drawer';

import { replaceRoute,popRoute } from '../../actions/route';
import styles from './styles';

const Item = Picker.Item;
const camera = require('../../../img/camera.png');

class NHForm extends Component {
  static propTypes = {
    popRoute: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
    replaceRoute: React.PropTypes.func,
  }
  constructor(props) {
    super(props);
    this.app=this.props.app,
    this.state={
      email:'',
      password:''
    }
  }
  popRoute() {
    this.props.popRoute();
  }
  replaceRoute(route) {
    this.props.replaceRoute(route);
  }
  _createUser(){
    var userData={email:this.state.email,password:this.state.password}
    this.app.service('users').create(userData)
    .then((result) => {
      this.app.authenticate({
        type: 'local',
        email: this.state.email,
        password: this.state.password
      }).then(this.props.navigator.pop())
        .catch(error => {
          console.log(error);
        });
    })
 }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Title>Sign Up</Title>
          <Button transparent onPress={() => this.popRoute()}>
            <Icon name="ios-power" />
          </Button>
        </Header>
        <Content>
          <TouchableOpacity>
            <Thumbnail size={80} source={camera} style={{ alignSelf: 'center', marginTop: 20, marginBottom: 10 }} />
          </TouchableOpacity>
          <List>
            <ListItem>
              <InputGroup>
                <Input inlineLabel label="First Name" placeholder="John" />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Input inlineLabel label="Last Name" placeholder="Doe" />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="ios-person" style={{ color: '#0A69FE' }} />
                <Input
                  placeholder="Email"
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                />
              </InputGroup>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="ios-unlock" style={{ color: '#0A69FE' }} />
                <Input
                  placeholder="Password"
                  secureTextEntry
                  onChangeText={(password)=>this.setState({password})}
                  value={this.state.password}
                />
              </InputGroup>
            </ListItem>
          </List>
          <Button style={{ alignSelf: 'center', marginTop: 20, marginBottom: 20 }} onPress={() => this._createUser()}>Sign Up</Button>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    replaceRoute: route => dispatch(replaceRoute(route)),
    popRoute: () => dispatch(popRoute()),
  };
}

export default connect(null, bindAction)(NHForm);
