
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity,ListView } from 'react-native';
import { Container, Header, Title, Content, Text, Button, Icon,View } from 'native-base';

import { openDrawer } from '../../actions/drawer';
import { popRoute } from '../../actions/route';
import styles from './styles';

var api={
  getPostsentiments(){
    return fetch('http://192.168.23.1:3030/postsentiments',{method:"GET"})
        .then((response) => response.json())
         .then((responseJson) => {
            return responseJson.data;
         })
         .catch((error) => {
         console.error(error);
        });
  }
}
var ds = new ListView.DataSource({rowHasChanged:(r1,r2)=>r1!=r2});
class History extends Component {
  static propTypes = {
    popRoute: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
    name: React.PropTypes.string,
  }
  constructor(props){
    super(props);
    this.state={
      postsentiment:[],
    }
  }
  componentWillMount(){
    api.getPostsentiments().then((response)=>{
      console.log('ObjectId => ',response);
      this.setState({
        postsentiment:response,
      })
    });
  }
  popRoute() {
    this.props.popRoute();
  }
  renderRow(rowData,selectionId,rowID){
    return(

          <View style={styles.Itemlistview}>
              <View>
               <Text>{rowData.createdAt.substring(0,10)} {rowData.createdAt.substring(11,19)}</Text>
              </View>
              <View>
               <Text>{rowData.author}</Text>
              </View>
              <View>
               <Text>{rowData.keyword}</Text>
              </View>
              <View>
               <Text>{rowData.message}</Text>
              </View>
          </View>
    )
  }

  render() {
    const { props: { name } } = this;
    return (
      <Container style={styles.container}>
        <Header>

          <Title>History</Title>

          <Button transparent onPress={this.props.openDrawer}>
            <Icon name="ios-menu" />
          </Button>
        </Header>

        <Content>
          <ListView
            dataSource={ds.cloneWithRows(this.state.postsentiment)}
            renderRow={this.renderRow.bind(this)}
          />
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    openDrawer: () => dispatch(openDrawer()),
    popRoute: () => dispatch(popRoute()),
  };
}
function mapStateToProps(state) {
  return {
    name: state.user.name,

  };
}

export default connect(mapStateToProps, bindAction)(History);
