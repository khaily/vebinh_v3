
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { TouchableOpacity,Alert } from 'react-native';
import { Container, Header, Title, Content, Button, Icon, List, ListItem, InputGroup, Input, Picker, Text, View, Thumbnail,Radio  } from 'native-base';

import { popRoute } from '../../actions/route';
import styles from './styles';

class EditPost extends Component {

  static propTypes = {
    popRoute: React.PropTypes.func,
    name: React.PropTypes.string,
  }
  constructor(props){
    super(props);
    this.state={
      postsentiment:this.props.data,
      mydata:[],
      radio1: false,
      radio2: false,
      radio3: false,
      radio4: true,
      newsentiment:'10',
      keyword:'',
      author:this.props.name
    }
  }
  _Alert(){
    Alert.alert(
      'Save Post Sentiment',
      'You wanna save this Post Sentiment?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this._createPostSentiment()}
      ]
    )
  }
  _createPostSentiment(){
      var mydata={
        postid:this.state.postsentiment._id,
        newsentiment:this.state.newsentiment,
        keyword:this.state.keyword,
        sentiment:this.state.postsentiment.sentiment,
        author: this.state.author,
        message:this.state.postsentiment.message
      }
    console.log("_id type =>",typeof this.state.postsentiment._id)
    console.log("mydata =>",mydata);
   fetch('http://192.168.23.1:3030/postsentiments', {
      method: 'POST',
        headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
       },
       body: JSON.stringify(mydata)
    }).then((response)=>response.json())
      .then((responseJson)=>{
        console.log("Post Respone =>",JSON.stringify(responseJson) )
        return responseJson;
      })
      .done()
 }
  popRoute() {
    this.props.popRoute();
  }
  toggleRadio1() {
    this.setState({
      radio1: true,
      radio2: false,
      radio3: false,
      radio4: false,
      newsentiment:'0',
      keyword:''
    });
  }

  toggleRadio2() {
    this.setState({
      radio1: false,
      radio2: true,
      radio3: false,
      radio4: false,
      newsentiment:'-1'
    });
  }

  toggleRadio3() {
    this.setState({
      radio1: false,
      radio2: false,
      radio3: true,
      radio4: false,
      newsentiment:'1'
    });
  }

  toggleRadio4() {
    this.setState({
      radio1: false,
      radio2: false,
      radio3: false,
      radio4: true,
      newsentiment:'10'
    });
  }
  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Button transparent onPress={() => this.popRoute()}>
            <Icon name="ios-arrow-back" />
          </Button>

          <Title>Edit PostSentiment</Title>
        </Header>

        <Content>

          <List>
            <ListItem>
              <InputGroup iconRight disabled>
                <Input inlineLabel label="Post ID:" placeholder={this.state.postsentiment._id} />
              </InputGroup>
            </ListItem>
            <ListItem>
              <Text numberOfLines={7}>
                Message: {this.state.postsentiment.message}
              </Text>
            </ListItem>
            <ListItem>
              <List>
                <ListItem button onPress={() => this.toggleRadio1()} >
                  <Radio selected={this.state.radio1} onPress={() => this.toggleRadio1()} />
                  <Text >0</Text>
                </ListItem>
                <ListItem button onPress={() => this.toggleRadio2()} >
                  <Radio selected={this.state.radio2} onPress={() => this.toggleRadio2()} />
                  <Text>-1</Text>
                </ListItem>
                <ListItem button onPress={() => this.toggleRadio3()} >
                  <Radio selected={this.state.radio3} onPress={() => this.toggleRadio3()} />
                  <Text>1</Text>
                </ListItem>
                <ListItem button onPress={() => this.toggleRadio4()} >
                  <Radio selected={this.state.radio4} onPress={() => this.toggleRadio4()} />
                  <Text>10</Text>
                </ListItem>
              </List>
            </ListItem>
            <ListItem>
              <InputGroup>
                <Icon name="ios-key" style={{ color: '#0A69FE' }} />
                <Input
                  placeholder="Key-Word"
                  onChangeText={(keyword)=>this.setState({keyword})}
                  value={this.state.keyword}
                />
              </InputGroup>
            </ListItem>

          </List>
          <Button style={{ alignSelf: 'center', marginTop: 20, marginBottom: 20 }} onPress={() => this._Alert()}>Save</Button>
        </Content>
      </Container>
    );
  }
}

function bindAction(dispatch) {
  return {
    popRoute: () => dispatch(popRoute()),

  };
}

function mapStateToProps(state) {
  return {
    name: state.user.name,
  };
}

export default connect(mapStateToProps, bindAction)(EditPost);
