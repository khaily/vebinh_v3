'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('postsentiment service', function() {
  it('registered the postsentiments service', () => {
    assert.ok(app.service('postsentiments'));
  });
});
