'use strict';

module.exports = function(app) {
  return function(req, res, next) {
    const body = req.body;

    // Get the user service and `create` a new user
    app.service('postsentiments').create({
      postid: body.postid,
      sentiment:body.sentiment,
      newsentiment:body.newsentiment,
      author:body.author,
      keyword:body.keyword,
      message: body.message,
    })
    // On errors, just call our error middleware
    .catch(next);
  };
};
