'use strict';

const service = require('feathers-mongoose');
const postsentiment = require('./postsentiment-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: postsentiment,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/postsentiments', service(options));

  // Get our initialize service to that we can bind hooks
  const postsentimentService = app.service('/postsentiments');

  // Set up our before hooks
  postsentimentService.before(hooks.before);

  // Set up our after hooks
  postsentimentService.after(hooks.after);
};
