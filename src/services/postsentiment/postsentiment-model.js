'use strict';

// postsentiment-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postsentimentSchema = new Schema({


  postid: { type:String, required:true},
  sentiment:{ type:String, required:false},
  newsentiment:{type: String,required:true},
  author:{type:String, required:true},
  keyword:{ type:String, required:false},
  message: { type: String, required: false },

  createdAt: { type: Date, 'default': Date.now },
  updatedAt: { type: Date, 'default': Date.now }
});

const postsentimentModel = mongoose.model('postsentiment', postsentimentSchema);

module.exports = postsentimentModel;
